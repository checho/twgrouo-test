<?php

use App\Http\Controllers\Api\UbicacionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(UbicacionController::class)->prefix('ubicaciones')->middleware('auth:sanctum')->group(function () {
    Route::get('/', 'index');
    Route::post('/crear', 'store');
    Route::get('/{ubicacion}', 'show');
    Route::put('/{ubicacion}', 'update');
    Route::delete('/{ubicacion}', 'destroy');
});
