<?php

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\LogoutResponse as FortifyLogoutResponse;

class LogoutResponse implements FortifyLogoutResponse
{
    function toResponse($request)
    {
        return response()->json(['data' => [
            'mensaje' => "Se ha cerrado la sesión exitosamente"
        ]]);
    }
}