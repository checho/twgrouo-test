<?php

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\LoginResponse as FortifyLoginResponse;

class LoginResponse implements FortifyLoginResponse
{
    function toResponse($request)
    {
        return response()->json(['data' => [
            'user' => $request->user()
        ]]);
    }
}