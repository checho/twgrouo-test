<?php

namespace App\Http\Controllers\Api;

use App\Models\Ubicacion;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUbicacionRequest;
use App\Http\Requests\UpdateUbicacionRequest;
use App\Modulos\Ubicacion\Interfaces\UbicacionInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UbicacionController extends Controller
{

    protected $ubicacionInterface;

    /**
     * Constructor de la clase
     *
     * @param UbicacionInterface $ubicacionInterface
     *
     */
    public function __construct(UbicacionInterface $ubicacionInterface)
    {
        $this->ubicacionInterface = $ubicacionInterface;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $datos = $this->ubicacionInterface->index();

        return  $datos['success']
            ? response()->json(
                $datos,
                $datos['code']
            )
            : response()->json($datos, $datos['code']);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUbicacionRequest $request)
    {
        $ubicacion = $this->ubicacionInterface->store($request);

        return  $ubicacion['success']
            ? response()->json(
                $ubicacion,
                $ubicacion['code']
            )
            : response()->json($ubicacion, $ubicacion['code']);
    }

    /**
     * Display the specified resource.
     */
    public function show($ubicacionId)
    {
        $ubicacionRepo = $this->ubicacionInterface->show($ubicacionId);

        return  $ubicacionRepo['success']
            ? response()->json(
                $ubicacionRepo,
                $ubicacionRepo['code']
            )
            : response()->json($ubicacionRepo, $ubicacionRepo['code']);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUbicacionRequest $request, Ubicacion $ubicacion)
    {
        $ubicacion = $this->ubicacionInterface->update($request, $ubicacion);

        return  $ubicacion['success']
            ? response()->json(
                $ubicacion,
                $ubicacion['code']
            )
            : response()->json($ubicacion, $ubicacion['code']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Ubicacion $ubicacion)
    {
        $ubicacion = $this->ubicacionInterface->destroy($ubicacion);

        return  $ubicacion['success']
            ? response()->json(
                $ubicacion,
                $ubicacion['code']
            )
            : response()->json($ubicacion, $ubicacion['code']);
    }
}
