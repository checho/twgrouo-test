<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUbicacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'longitud' => 'required',
            'latitud' => 'required',
            'region' => 'sometimes|required|nullable',
            'comuna' => 'sometimes|required|nullable',
            'direccion' => 'sometimes|required|nullable',
            'user_id' => 'required',
        ];
    }
}
