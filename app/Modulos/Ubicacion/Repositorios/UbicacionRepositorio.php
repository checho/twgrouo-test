<?php

namespace App\Modulos\Ubicacion\Repositorios;

use App\Models\Ubicacion;
use App\Http\Requests\StoreUbicacionRequest;
use App\Http\Requests\UpdateUbicacionRequest;

class UbicacionRepositorio
{
    public function index()
    {
        return Ubicacion::orderBy('created_at', 'DESC')
            ->get();
    }

    public function store(StoreUbicacionRequest $request)
    {
        $ubicacion = new Ubicacion();

        $ubicacion->longitud = $request->input('longitud');
        $ubicacion->latitud = $request->input('latitud');
        $ubicacion->region = $request->input('region', null);
        $ubicacion->comuna = $request->input('comuna', null);
        $ubicacion->direccion = $request->input('direccion', null);
        $ubicacion->user_id = $request->input('user_id');

        return $ubicacion->save();
    }

    public function show($ubicacionId)
    {
        return Ubicacion::find($ubicacionId);
    }

    public function update(UpdateUbicacionRequest $request, Ubicacion $ubicacion)
    {
        $ubicacion->longitud = $request->input('longitud');
        $ubicacion->latitud = $request->input('latitud');
        $ubicacion->region = $request->input('region', null);
        $ubicacion->comuna = $request->input('comuna', null);
        $ubicacion->direccion = $request->input('direccion', null);
        $ubicacion->user_id = $request->input('user_id');

        return $ubicacion->save();
    }

    public function destroy(Ubicacion $ubicacion)
    {
        return $ubicacion->delete();
    }
}
