<?php

namespace App\Modulos\Ubicacion\Decoradores;

use App\Models\Ubicacion;
use App\Http\Requests\StoreUbicacionRequest;
use App\Http\Requests\UpdateUbicacionRequest;
use App\Modulos\Ubicacion\Interfaces\UbicacionInterface;
use App\Modulos\Ubicacion\Repositorios\UbicacionRepositorio;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UbicacionDecorador implements UbicacionInterface
{

    protected $datosRepositorio;


    public function __construct(UbicacionRepositorio $datosRepositorio)
    {
        $this->datosRepositorio = $datosRepositorio;
    }

    public function index()
    {
        try {
            $ubicaciones = $this->datosRepositorio->index();

            throw_if(
                blank($ubicaciones),
                new \Exception('No hay ubicaciones registradas', 200)
            );

            // throw new \Exception('Test error !!', 500);

            return  [
                'success' => true,
                'code' => 200,
                'data' => [
                    'ubicaciones' => $ubicaciones,
                ]
            ];
        } catch (\Throwable $e) {
            \Log::error($e);

            return [
                'success' => ($e->getCode() !== 200) ? false : true,
                'code' => $e->getCode(),
                'data' => ['mensaje' => $e->getMessage()]
            ];
        }
    }

    public function store(StoreUbicacionRequest $request)
    {
        try {
            $ubicacion = $this->datosRepositorio->store($request);

            throw_if(
                blank($ubicacion),
                new \Exception('Error al crear la ubicación en base de datos')
            );

            return  [
                'success' => true,
                'code' => 200,
                'data' => [
                    'ubicacion' => $ubicacion,
                    'mensaje' => "Ubicación guardada exitosamente"
                ]
            ];
        } catch (\Throwable $e) {
            \Log::error($e);

            return [
                'success' => false,
                'code' => 500,
                'data' => ['mensaje' => $e->getMessage()]
            ];
        }
    }

    public function show($ubicacionId)
    {
        try {
            $ubicacion = $this->datosRepositorio->show($ubicacionId);

            throw_if(
                blank($ubicacion),
                new \Exception('No se pudo encontrar la ubicación buscada', 404)
            );

            // throw new \Exception('Test error !!', 500);

            return  [
                'success' => true,
                'code' => 200,
                'data' => [
                    'ubicacion' => $ubicacion,
                ]
            ];
        } catch (\Throwable $e) {
            \Log::error($e);

            return [
                'success' => ($e->getCode() !== 200) ? false : true,
                'code' => $e->getCode(),
                'data' => ['mensaje' => $e->getMessage()]
            ];
        }
    }

    public function update(UpdateUbicacionRequest $request, Ubicacion $ubicacion)
    {
        try {
            $ubicacion = $this->datosRepositorio->update($request, $ubicacion);

            throw_if(
                blank($ubicacion),
                new \Exception('Error al editar la ubicación en base de datos')
            );

            return  [
                'success' => true,
                'code' => 200,
                'data' => [
                    'ubicacion' => $ubicacion,
                    'mensaje' => "Ubicación actualizada exitosamente"
                ]
            ];
        } catch (\Throwable $e) {
            \Log::error($e);

            return [
                'success' => false,
                'code' => 500,
                'data' => ['mensaje' => $e->getMessage()]
            ];
        }
    }

    public function destroy(Ubicacion $ubicacion)
    {
        try {
            $ubicacion = $this->datosRepositorio->destroy($ubicacion);

            throw_if(
                blank($ubicacion),
                new \Exception('Error al eliminar la ubicación en base de datos')
            );

            return  [
                'success' => true,
                'code' => 200,
                'data' => [
                    'mensaje' => "Ubicación eliminada exitosamente"
                ]
            ];
        } catch (\Throwable $e) {
            \Log::error($e);

            return [
                'success' => false,
                'code' => 500,
                'data' => ['mensaje' => $e->getMessage()]
            ];
        }
    }
}
