<?php

namespace App\Modulos\Ubicacion\Interfaces;

use App\Models\Ubicacion;
use App\Http\Requests\StoreUbicacionRequest;
use App\Http\Requests\UpdateUbicacionRequest;

interface UbicacionInterface
{
    public function index();
    public function store(StoreUbicacionRequest $request);
    public function show($ubicacionId);
    public function update(UpdateUbicacionRequest $request, Ubicacion $ubicacion);
    public function destroy(Ubicacion $ubicacion);
}
