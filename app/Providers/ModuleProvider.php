<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modulos\Ubicacion\Interfaces\UbicacionInterface;
use App\Modulos\Ubicacion\Decoradores\UbicacionDecorador;

class ModuleProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(UbicacionInterface::class, UbicacionDecorador::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
