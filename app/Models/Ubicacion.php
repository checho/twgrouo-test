<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ubicaciones';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'longitud',
        'latitud',
        'region',
        'comuna',
        'direccion',
        'user_id',
    ];

    function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
